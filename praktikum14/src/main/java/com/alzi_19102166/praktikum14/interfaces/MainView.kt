package com.alzi_19102166.praktikum14.interfaces

import com.alzi_19102166.praktikum14.model.Login
import com.alzi_19102166.praktikum14.model.Quote

interface MainView {
    fun showMessage(messsage : String)
    fun resultQuote(data: ArrayList<Quote>)
    fun resultLogin(data: Login)
}
