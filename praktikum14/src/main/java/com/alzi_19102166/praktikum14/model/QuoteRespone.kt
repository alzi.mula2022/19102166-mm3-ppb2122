package com.alzi_19102166.praktikum14.model

data class QuoteResponse(
    val quotes: ArrayList<Quote>)
