package com.alzi_19102166.praktikum14.ui.classquotes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.alzi_19102166.praktikum14.TokenPref
import com.alzi_19102166.praktikum14.interfaces.MainView
import com.alzi_19102166.praktikum14.adapter.QuoteAdapter
import com.alzi_19102166.praktikum14.api.MainPresenter
import com.alzi_19102166.praktikum14.databinding.FragmentClassQuotesBinding
import com.alzi_19102166.praktikum14.interfaces.CoroutineContextProvider
import com.alzi_19102166.praktikum14.model.Login
import com.alzi_19102166.praktikum14.model.Quote
import com.alzi_19102166.praktikum14.model.Token

class ClassQuotesFragment : Fragment(), MainView {
    private lateinit var presenter: MainPresenter
    private var quotes: MutableList<Quote> = mutableListOf()
    private lateinit var adapter: QuoteAdapter
    private lateinit var tokenPref: TokenPref
    private lateinit var token: Token

    private var _binding: FragmentClassQuotesBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState:
        Bundle?
    ): View {
        _binding = FragmentClassQuotesBinding.inflate(inflater, container, false)
        val root: View = binding.root
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = FragmentClassQuotesBinding.bind(view)

        binding.recyclerviewClassQuotes.layoutManager = LinearLayoutManager(activity)
        tokenPref = TokenPref(requireActivity())
        token = tokenPref.getToken()
        adapter = QuoteAdapter(requireActivity())
        binding.recyclerviewClassQuotes.adapter = adapter

        presenter = MainPresenter(this, CoroutineContextProvider())
        binding.progressbar.visibility = View.VISIBLE

        presenter.getClassQuotes(token.token)
        binding.swiperefresh.setOnRefreshListener {
            binding.progressbar.visibility = View.INVISIBLE
            presenter.getMyQuotes(token.token)
        }
    }
    override fun onResume() {
        super.onResume()
        presenter.getClassQuotes(token.token)
    }

    override fun showMessage(messsage: String) {
        Toast.makeText(requireActivity(),messsage, Toast.LENGTH_SHORT).show()
    }
    override fun resultQuote(data: ArrayList<Quote>) {
        quotes.clear()
        adapter.listQuotes = data
        quotes.addAll(data)
        adapter.notifyDataSetChanged()
        binding.progressbar.visibility = View.INVISIBLE
        binding.swiperefresh.isRefreshing = false
    }
    override fun resultLogin(data: Login) {
    }
}

